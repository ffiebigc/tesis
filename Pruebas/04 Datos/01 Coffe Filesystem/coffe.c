#include <stdio.h> /* Para printf */
#include "contiki.h"
#include "cfs/cfs.h"
#include "dev/button-sensor.h"
#include "sys/clock.h"

PROCESS(cfs_process, "Coffe Test");
AUTOSTART_PROCESSES(&cfs_process);

PROCESS_THREAD(cfs_process, ev, data)
{

	PROCESS_BEGIN();
	SENSORS_ACTIVATE(button_sensor);

 	static int fd;

	while (1)
	{
		PROCESS_WAIT_EVENT_UNTIL(ev == sensors_event && data == &button_sensor);
		char buf[40] = "";
		sprintf(buf, "Been alive %lu seconds", clock_seconds());
		fd = cfs_open("test", CFS_READ | CFS_WRITE);
		if(fd >= 0) 
		{
   			cfs_write(fd, buf, sizeof(buf));
   			cfs_seek(fd, 0, CFS_SEEK_SET);
   			cfs_read(fd, buf, sizeof(buf));
   			printf("Read file: %s\n", buf);
   			cfs_close(fd);
 		}
	}
	PROCESS_END();
}