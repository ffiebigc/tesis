#include <stdio.h> /* Para printf */
#include "contiki.h"
#include "uip.h"

PROCESS(staticipv4_process, "Static IPv4 Test");
AUTOSTART_PROCESSES(&staticipv4_process);

PROCESS_THREAD(staticipv4_process, ev, data)
{
	
	PROCESS_BEGIN();

	// REVISAR IP Y NETMASK ACTUAL
	static uip_ipaddr_t hostaddr, hostnetmask;
    uip_gethostaddr(&hostaddr);
    uip_getnetmask(&hostnetmask);
    printf("\nObtener valores actuales:\n");
    printf("IP actual de Interfaz: %u.%u.%u.%u\n", hostaddr.u8[0], hostaddr.u8[1], hostaddr.u8[2], hostaddr.u8[3]);
    printf("Mascara actual de Sub Red: %u.%u.%u.%u\n", hostnetmask.u8[0], hostnetmask.u8[1], hostnetmask.u8[2], hostnetmask.u8[3]);
    printf("-------------\n\n");

    // SETEAR IP Y NETMASK
    printf("Setear nuevos valores:\n");
	static uip_ipaddr_t address, netmask;
	uip_ipaddr(&address, 192,168,0,23);
	uip_ipaddr(&netmask, 255,255,255,0);
	uip_sethostaddr(&address);
	uip_setnetmask(&netmask);
	uip_over_mesh_set_net(&address, &netmask);
	printf("Seteando \nIP: %u.%u.%u.%u\n", address.u8[0], address.u8[1], address.u8[2], address.u8[3]);
	printf("Mascara de Sub Red: %u.%u.%u.%u\n", netmask.u8[0], netmask.u8[1], netmask.u8[2], netmask.u8[3]);
	printf("-------------\n\n");

	// REVISAR NUEVOS VALORES
	printf("Obtener valores actualizados:\n");
	static uip_ipaddr_t hostaddr2, netmask2;
    uip_gethostaddr(&hostaddr2);
    uip_getnetmask(&netmask2);
    printf("IP de Interfaz: %u.%u.%u.%u\n", hostaddr2.u8[0], hostaddr2.u8[1], hostaddr2.u8[2], hostaddr2.u8[3]);
    printf("Mascara de Sub Red: %u.%u.%u.%u\n", netmask2.u8[0], netmask2.u8[1], netmask2.u8[2], netmask2.u8[3]);
    

    /*
    *
    *
    * 	TODO_ SETEAR IP FIJA AL BOOTEAR ??
    *
    * 	IP es fija desde un principio, se setea segun 
    * 	uip_ipaddr(&hostaddr, 172,16, linkaddr_node_addr.u8[0],linkaddr_node_addr.u8[1]);
    *   uip_ipaddr(&netmask, 255,255,0,0);
    *	en contiki-sky-main.c
    *
    *
     */

	PROCESS_END();
}
