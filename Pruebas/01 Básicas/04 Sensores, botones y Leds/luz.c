#include <stdio.h> /* Para printf */
#include "contiki.h"
#include "dev/light-sensor.h"

PROCESS(luz_process, "Luz");
AUTOSTART_PROCESSES(&luz_process);

PROCESS_THREAD(luz_process, ev, data)
{
	PROCESS_BEGIN();
	/* Los sensores deben ser activados entre PROCESS_BEGIN y PROCESS END */
	SENSORS_ACTIVATE(light_sensor);
	printf("Luz: \%u\n", light_sensor.value(0));

	PROCESS_END();
}
