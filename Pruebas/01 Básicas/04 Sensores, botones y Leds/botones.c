#include <stdio.h> /* Para printf */
#include "contiki.h"
#include "dev/light-sensor.h"
#include "dev/button-sensor.h"
#include "dev/leds.h"

PROCESS(botones_process, "Botones");
AUTOSTART_PROCESSES(&botones_process);

PROCESS_THREAD(botones_process, ev, data)
{
	PROCESS_BEGIN();
	/* Los sensores deben ser activados entre PROCESS_BEGIN y PROCESS END */
	SENSORS_ACTIVATE(light_sensor);
	SENSORS_ACTIVATE(button_sensor);

	while (1)
	{
		PROCESS_WAIT_EVENT_UNTIL(ev == sensors_event && data == &button_sensor); // Esperar que el boton sea presionado
		printf("Luz: \%u\n", light_sensor.value(0));
		leds_toggle(LEDS_ALL);
	}
	
	PROCESS_END();
}
