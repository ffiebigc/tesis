#include <stdio.h> /* Para printf */
#include "contiki.h"
#include "dev/leds.h"
#include "sys/etimer.h"

PROCESS(timer_process, "Timer Test");
AUTOSTART_PROCESSES(&timer_process);

PROCESS_THREAD(timer_process, ev, data)
{
	
	PROCESS_BEGIN();
	static struct etimer et;

	while (1)
	{
		etimer_set(&et, CLOCK_SECOND);
		PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
		leds_toggle(LEDS_ALL);
		printf("LEDS should toggle now\n");
	}	
	PROCESS_END();
}
