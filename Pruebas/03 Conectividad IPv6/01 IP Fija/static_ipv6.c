#include <stdio.h> /* Para printf */
#include "contiki.h"
#include "uip.h"
#include "uip-ds6.h"
#include "uip-debug.h"

PROCESS(staticipv6_process, "Static IPv6 Test");
AUTOSTART_PROCESSES(&staticipv6_process);

PROCESS_THREAD(staticipv6_process, ev, data)
{
	
	PROCESS_BEGIN();

    int i;
    static uip_netif_state state;
    PRINTF("Client IPv6 addresses: ");
    for(i = 0; i < UIP_CONF_NETIF_MAX_ADDRESSES; i++) {
        state = uip_netif_physical_if.addresses[i].state;
        PRINTF(state);
        /*if(state == TENTATIVE || state == PREFERRED) {
            PRINT6ADDR(&uip_netif_physical_if.addresses[i].ipaddr);
            PRINTF("\n");
        }*/
   }

	PROCESS_END();
}
