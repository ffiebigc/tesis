cd ~
mkdir mspgcc_install
cd mspgcc_install

#Dependencies
sudo apt-get install patch ncurses-dev build-essential bison flex zlib1g-dev sed automake gawk mawk libusb-1.0.0 libusb-1.0.0-dev dos2unix srecord


#Downloads:

wget http://sourceforge.net/projects/mspgcc/files/mspgcc/DEVEL-4.7.x/mspgcc-20120911.tar.bz2
wget http://sourceforge.net/projects/mspgcc/files/msp430mcu/msp430mcu-20130321.tar.bz2
wget http://sourceforge.net/projects/mspgcc/files/msp430-libc/msp430-libc-20120716.tar.bz2
wget http://ftpmirror.gnu.org/binutils/binutils-2.22.tar.bz2
wget http://ftp.gnu.org/pub/gnu/gcc/gcc-4.7.0/gcc-4.7.0.tar.bz2
wget http://ftp.gnu.org/pub/gnu/gdb/gdb-7.2a.tar.bz2

wget http://sourceforge.net/p/mspgcc/bugs/_discuss/thread/fd929b9e/db43/attachment/0001-SF-357-Shift-operations-may-produce-incorrect-result.patch
wget http://sourceforge.net/p/mspgcc/bugs/352/attachment/0001-SF-352-Bad-code-generated-pushing-a20-from-stack.patch

#GDB patch download
wget -O gdb.patch https://sourceware.org/git/?p=gdb.git;a=patch;h=7f62f13c2b535c6a23035407f1c8a36ad7993dec

#Uninstall texinfo 5.x and install oldversion 4.x
wget http://ftp.br.debian.org/debian/pool/main/t/texinfo/texinfo_4.13a.dfsg.1-10_amd64.deb
sudo dpkg -r texinfo
sudo dpkg -i texinfo_4.13a.dfsg.1-10_amd64.deb

#Extract

tar xvfj mspgcc-20120911.tar.bz2
tar xvfj binutils-2.22.tar.bz2 
tar xvfj gcc-4.7.0.tar.bz2  
tar xvfj gdb-7.2a.tar.bz2
tar xvfj msp430mcu-20130321.tar.bz2 
tar xvfj msp430-libc-20120716.tar.bz2 


#Create build folders
mkdir build
cd build
mkdir binutils 
mkdir gcc
mkdir gdb
cd ..

#Binutils install

cd binutils-2.22
patch -p1<../mspgcc-20120911/msp430-binutils-2.22-20120911.patch
cd ../build/binutils 
../../binutils-2.22/configure --target=msp430 --prefix=/usr/local/msp430 2>&1 | tee co
make 2>&1 | tee mo
sudo make install 2>&1 | tee moi

#gcc
cd ../../gcc-4.7.0
patch -p1 < ../mspgcc-20120911/msp430-gcc-4.7.0-20120911.patch
patch -p1< ../0001-SF-352-Bad-code-generated-pushing-a20-from-stack.patch
patch -p1< ../0001-SF-357-Shift-operations-may-produce-incorrect-result.patch
./contrib/download_prerequisites
#ira-int.h replace
cd gcc
rm ira-int.h
wget -O ira-int.h https://gcc.gnu.org/viewcvs/gcc/branches/gcc-4_7-branch/gcc/ira-int.h?revision=191605&view=co&pathrev=191605

cd ../../build/gcc 
../../gcc-4.7.0/configure --target=msp430 --enable-languages=c,c++ --prefix=/usr/local/msp430 2>&1 | tee co
make 2>&1 | tee mo
#The previous command take a while (tens of minutes), continue to install mspdebug or Eclipse Luna if you don't want to wait.
sudo make install 2>&1 | tee moi

#If you are error-free at this point, congratulations!!! you can continue.
#Otherwise, sorry, you are on your own... the 43oh.com guys can help!!!

#Update PATH
export PATH=/usr/local/msp430/bin/:$PATH
sudo sed -e '/^PATH/s/"$/:\/usr\/local\/msp430\/bin"/g' -i /etc/environment

#Check msp430-gcc, must return 4.7.0 20120322
msp430-gcc --version

#Install gdb
cd ../../gdb-7.2
patch -p1 < ../mspgcc-20120911/msp430-gdb-7.2a-20111205.patch
patch -p1< ../gdb.patch
cd ../build/gdb
../../gdb-7.2/configure --target=msp430 --prefix=/usr/local/msp430 2>&1 | tee co 
make 2>&1 | tee mo
sudo make install 2>&1 | tee moi 
#If error verify you have old version texinfo package 

#Check gdb installation:
msp430-gdb --version
#Must return 7.2


#msp430mcu and msp430-libc
cd ../../msp430mcu-20130321/
sudo MSP430MCU_ROOT=`pwd` ./scripts/install.sh /usr/local/msp430 | tee so

cd ../msp430-libc-20120716/src/
make 2>&1 | tee mo
sudo PATH=$PATH make PREFIX=/usr/local/msp430 install 2>&1 | tee moi
cd ../..

#If needed, remove old version of texinfo and install the new one
sudo dpkg -r texinfo
sudo apt-get install texinfo
