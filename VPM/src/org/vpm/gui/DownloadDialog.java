package org.vpm.gui;

import org.vpm.HTTPDownload;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Esta clase se encarga de mostrar el dialogo de descarga, el cual a través de la clase HTTPDownload se encarga de
 * descargar y descomprimir Contiki.
 */
public class DownloadDialog extends JDialog implements PropertyChangeListener {
    private JPanel contentPane;
    private JButton buttonOK;
    public JProgressBar progressBar;
    private JLabel top;
    private JLabel bottom;
    private MainWindow parent;

    /**
     * Instancia la ventana
     * @param parent el componente padre
     */
    public DownloadDialog(MainWindow parent) {
        this.parent = parent;
        setTitle("Descargando Contiki");
        setTopText("Descargando, espere por favor...");
        setLocationRelativeTo(parent);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        progressBar.setMaximum(100);
        progressBar.setValue(0);
        progressBar.setIndeterminate(true);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });


// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        HTTPDownload contikiDownload = new HTTPDownload(this,
                "https://github.com/contiki-os/contiki/archive/3.0.zip", "3.0.zip");
        contikiDownload.addPropertyChangeListener(this);
        contikiDownload.execute();

        buttonOK.setEnabled(false);
        pack();
        setMinimumSize(new Dimension(400, 200));
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    /**
     * Cierra la ventana al presionar el boton "Listo" del dialogo, solo estará disponible cuando se termine de
     * descargar y descomprimir contiki.
     */
    private void onOK() {
        dispose();
    }

    /**
     * Si se cancela el proceso este metodo se encarga de cerrar la ventana.
     */
    private void onCancel() {
        sendCloseSignal();
        dispose();
    }

    /**
     * Cambia el texto del label superior del dialogo
     * @param text
     */
    public void setTopText(String text) {
        top.setText(text);
    }

    /**
     * Cambia el texto del label inferior del dialogo
     * @param text
     */
    public void setBottomText(String text) {
        bottom.setText(text);
    }

    /**
     * Envia al padre la señal de que debe cerrarse.
     */
    public void sendCloseSignal() {
        parent.dispatchEvent(new WindowEvent(parent, WindowEvent.WINDOW_CLOSING));
    }

    /**
     * Habilita el boton "Listo" del dialgoo para poder continuar, solo estará habilitado cuando se termine de
     * descargar y descomprimir contiki
     * @param enabled el booloano representando el estado que se le asignara al boton
     */
    public void enableOkButton(boolean enabled) {
        buttonOK.setEnabled(enabled);
    }

    /**
     * Se lanza cuando una propiedad del elemento al que se está escuchando cambie.
     * @param e el evento de propiedad cambiada
     */
    @Override
    public void propertyChange(PropertyChangeEvent e) {
        if (e.getPropertyName().equals("progress")) {
            int progress = (Integer) e.getNewValue();
            progressBar.setStringPainted(true);
            progressBar.setIndeterminate(false);
            progressBar.setValue(progress);
        }
    }
}
