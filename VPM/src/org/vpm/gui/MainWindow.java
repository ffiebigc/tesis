package org.vpm.gui;

import org.vpm.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Esta es la clase principal, se encarga de mostrar la ventana incial del programa utiliza el archivo MainWindow.form
 * para montar la  interfaz grafica y en esta clase se definen los comportamientos de los componentes.
 * Created by felipe on 5/4/16.
 */
public class MainWindow extends JFrame{

    private boolean DEBUG = true;
    private JPanel rootPanel;
    private JList programsList;
    private JButton borrarSeleccionadoButton;
    private JButton agregarProgramaButton;
    private JList moteJList;
    private JPanel leftApplicationPanel;
    private JLabel moteTitleLabel;
    private JLabel moteDescLabel;
    private JLabel moteDevLabel;
    private JButton refrescarButton;
    private JList pgMoteList;
    private JTree pgProgramsTree;
    private JButton programarNodosButton;
    private JLabel pgStatusLabel;
    private JList pgResultList;
    private JLabel moteStatusLabel;
    private JTabbedPane tbPaneMain;
    private JLabel contikiStatusLabel;
    private JButton checkContikiButton;
    private SettingsHandler settingsHandler;
    private MoteFinder moteFinder;
    private FileTree appFileTree;
    private ArrayList<Mote> motes;

    /**
     * Inicializa la ventana principal del programa.
     */
    public MainWindow() {
        setTitle("Node Programmer");
        pack();
        setMinimumSize(new Dimension(800, 600));
        setLocationRelativeTo(null);
        setContentPane(rootPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        settingsHandler = new SettingsHandler();
        moteFinder = new MoteFinder();
        MoteRenderer moteRenderer = new MoteRenderer();
        //Menu
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        menu.setMnemonic(KeyEvent.VK_F);

        JMenuItem menuItem = new JMenuItem("Agregar Programa");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.ALT_MASK));
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agregarProgramaButton.doClick();
            }
        });
        menu.add(menuItem);

        menuItem = new JMenuItem("Reinstalar Contiki");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.ALT_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription(
                "Elemina y luego descarga contiki nuevamente.");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Si, reinstalar.",
                        "No"};
                int answer = JOptionPane.showOptionDialog(MainWindow.this,
                        "Ha elegido reinstalar Contiki el cual es esencial para el funcionamiento de este programa \n" +
                                "¿Está seguro que desea reinstalar?",
                        "Reinstalar Contiki",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[0]);
                if (answer == 0){
                    deleteDirectory(new File("contiki-3.0"));
                    findAndDownloadContiki(true);
                }
            }
        });
        menu.add(menuItem);
        menu.addSeparator();

        menuItem = new JMenuItem("Salir");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                closeFrame();
            }
        });
        menu.add(menuItem);
        menuBar.add(menu);

        menu = new JMenu("Acerca de..");
        menuItem = new JMenuItem("Información");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(MainWindow.this, "Herramienta de programación asistida de " +
                        "nodos tipo sky para redes de sensores.\n\n" +
                        "Algunos de los ejemplos de contiki no son funcionales" +
                        " en todas las \nplataformas debido a la diversidad de sus capacidades de hardware.\n\n"+
                        "Las licencias de codigos externos se encuentran documentadas \n" +
                        "en sus propios archivos fuente.\n\n" +
                        "Felipe Fiebig, Chile, 2016.", "Acerca de ...",
                        JOptionPane.INFORMATION_MESSAGE);

            }
        });
        menu.add(menuItem);
        menuBar.add(menu);
        this.setJMenuBar(menuBar);
        //Tab Aplicaciones
        programsList.setCellRenderer(new ProgramRenderer());
        programsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        moteJList.setCellRenderer(moteRenderer);
        moteJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        contikiStatusLabel.setOpaque(true);
        contikiStatusLabel.setBackground(Color.RED);
        //Tab nodos
        Font font = new Font(moteTitleLabel.getFont().getName(), Font.PLAIN, 16);
        moteTitleLabel.setFont(new Font(moteTitleLabel.getFont().getName(), Font.PLAIN, 22));
        moteDescLabel.setFont(font);
        moteDevLabel.setFont(font);
        moteStatusLabel.setFont(font);
        //Tab programar
        pgProgramsTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        pgMoteList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        pgMoteList.setCellRenderer(moteRenderer);
        pgResultList.setCellRenderer(new ResultRenderer());
        setVisible(true);

        // Checkear si contiki existe
        findAndDownloadContiki(false);

        agregarProgramaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame frame = (JFrame) SwingUtilities.getRoot((Component) e.getSource());
                final JFileChooser fc = new JFileChooser();
                fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
                fc.setAcceptAllFileFilterUsed(false);
                fc.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        if (f.getName().equals("symbols.c")){
                            return false;
                        }
                        else if(f.getName().endsWith(".c") || f.isDirectory()) {
                            return true;
                        }
                        return false;
                    }

                    @Override
                    public String getDescription() {
                        return "C Code Files (*.c)";
                    }
                });
                int returnValue = fc.showOpenDialog(frame);

                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    if (file != null) {
                        // Checkear si tiene un Makefile
                        File[] directoryFiles = file.getParentFile().listFiles();
                        boolean hasMakefile = false;
                        if (directoryFiles != null) {
                            for (int j = 0; j < directoryFiles.length; j++) {
                                if (directoryFiles[j].getName().equals("Makefile")) { hasMakefile = true; }
                            }
                        }
                        if (hasMakefile){
                            DefaultListModel<Program> listModel = (DefaultListModel<Program>)programsList.getModel();
                            Program program = new Program(file.getName(), file.getPath());
                            boolean alreadyAdded = false;
                            for (int i = 0; i<listModel.getSize(); i++){
                                if (listModel.getElementAt(i).getPath().equals(program.getPath())){
                                    alreadyAdded = true;
                                }
                            }
                            if (!alreadyAdded) {
                                listModel.addElement(program);
                                settingsHandler.appendProgram(program);
                            }
                        }else {
                            JOptionPane.showMessageDialog(frame, "No se encontró makefile del programa.",
                                    "Makefile no encontrado", JOptionPane.ERROR_MESSAGE);
                        }
                        if (DEBUG) {System.out.println("MW: Seleccionado: " + file);}
                    } else {
                        JOptionPane.showMessageDialog(frame, "Error al seleccionar archivo, intentelo nuevamente.",
                                "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
                populatePgProgramTree();
            }
        });

        borrarSeleccionadoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultListModel<Program> listModel = (DefaultListModel<Program>)programsList.getModel();
                int selectedIndex = programsList.getSelectedIndex();
                listModel.removeElementAt(selectedIndex);
                settingsHandler.removeProgram(selectedIndex);
                populatePgProgramTree();
            }
        });
        moteJList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                JList<Mote> list = (JList)e.getSource();
                Mote mote = list.getSelectedValue();
                if (mote != null){
                    moteTitleLabel.setText("<html><b><u>Mota Nº " + (list.getSelectedIndex()+1));
                    moteDescLabel.setText("<html><b>Descripción:</b> " + mote.getDescription());
                    moteDevLabel.setText("<html><b>Dispositivo:</b> " + mote.getDevice());
                    switch (mote.getStatus()) {
                        case Mote.WAITING:
                            moteStatusLabel.setText("<html><b>Estado:</b> En espera");
                            break;
                        case Mote.FAILED:
                            moteStatusLabel.setText("<html><b>Estado:</b> Presentó un error");
                            break;
                        case Mote.RETRY:
                            moteStatusLabel.setText("<html><b>Estado:</b> Presentó un error");
                            break;
                        case Mote.BEING_PROGRAMMED:
                            moteStatusLabel.setText("<html><b>Estado:</b> Siendo programada");

                    }
                }else {
                    moteTitleLabel.setText("");
                    moteDescLabel.setText("");
                    moteDevLabel.setText("");
                    moteStatusLabel.setText("");
                }
            }
        });
        refrescarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainWindow frame = (MainWindow) SwingUtilities.getRoot((Component) e.getSource());
                boolean shouldRefresh = true;
                int count = 0;
                DefaultListModel<Mote> listModel = (DefaultListModel) pgMoteList.getModel();
                for (int i = 0; i<listModel.getSize(); i++){
                    Mote local = listModel.getElementAt(i);
                    if (local.getStatus() == Mote.BEING_PROGRAMMED){
                        shouldRefresh = false;
                        count++;
                    }
                }
                if (shouldRefresh){
                    frame.populateMoteList();
                }else{
                    JOptionPane.showMessageDialog(frame, "No se puede refrescar, \naun hay "+count+
                            " nodo(s) siendo programado(s).", "Espere", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        programarNodosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainWindow frame = (MainWindow) SwingUtilities.getRoot((Component) e.getSource());
                Program program;
                if (pgProgramsTree.getSelectionCount() == 1 && pgMoteList.getSelectedIndices().length > 0) {
                    DefaultMutableTreeNode selectedNode =
                            (DefaultMutableTreeNode) pgProgramsTree.getLastSelectedPathComponent();
                    //setear programa
                    if (selectedNode.getUserObject().toString().endsWith(".c")) {
                        if (selectedNode.getUserObject().getClass() == Program.class) {
                            //Termina en .c y es de clase Program
                            program = (Program) selectedNode.getUserObject();

                        } else {
                            //Termina en .c y es de clase String
                            program = new Program(selectedNode.getUserObject().toString(),
                                    selectedNode.getParent().toString()+"/"+selectedNode.getUserObject().toString());
                        }
                        // Hilo de compilacion y carga
                        programarNodosButton.setEnabled(false);
                        pgStatusLabel.setText("Compilando ...");
                        new SwingWorker<Void, Void>(){
                            private boolean compileError = false;
                            private boolean loadError = false;
                            @Override
                            protected Void doInBackground(){
                                try{
                                    if (DEBUG) { System.out.println("MW: Compilando "+program.getName()+"..."); }
                                    int retVal = program.compile();

                                    //Cargar
                                    if (retVal == 0){
                                        if (DEBUG) { System.out.println("MW: Cargando "+program.getName()); }
                                        pgStatusLabel.setText("Programando ...");
                                        DefaultListModel<Mote> listModel = (DefaultListModel) pgMoteList.getModel();


                                        for (int i = 0; i < pgMoteList.getSelectedIndices().length; i++) {
                                            Mote local = listModel.getElementAt(pgMoteList.getSelectedIndices()[i]);
                                            int selectedIndex = pgMoteList.getSelectedIndices()[i];

                                            new SwingWorker<Void, Void>() {
                                                @Override
                                                protected Void doInBackground() {
                                                    if (local.getStatus() == Mote.RETRY ||
                                                            local.getStatus() == Mote.WAITING) {
                                                        local.setStatus(Mote.BEING_PROGRAMMED);
                                                        listModel.setElementAt(local, selectedIndex);
                                                        int uploadRetVal = local.program(program);
                                                        if (uploadRetVal == 1) {
                                                            JOptionPane.showMessageDialog(frame,
                                                                    "Contraseña ingresada es incorrecta",
                                                                    "Error", JOptionPane.ERROR_MESSAGE);

                                                        } else if (uploadRetVal != 0) {
                                                            loadError = true;
                                                        }
                                                        if (loadError) {
                                                            pgStatusLabel.setText("Error al cargar " + local.getDevice());
                                                            programarNodosButton.setEnabled(true);
                                                        }
                                                        listModel.setElementAt(local, selectedIndex);
                                                    }

                                                    return null;
                                                }
                                            }.execute();
                                        }
                                    } else {
                                        compileError = true;
                                    }
                                }catch (IOException ioe){
                                    compileError = true;
                                }catch (InterruptedException ie){
                                    compileError = true;
                                }
                                return null;
                            }

                            @Override
                            protected void done(){
                                if (loadError){
                                    pgStatusLabel.setText("Error al cargar.");
                                    programarNodosButton.setEnabled(true);
                                }
                                if (compileError){
                                    programarNodosButton.setEnabled(true);
                                    pgStatusLabel.setText("Error de compilación.");
                                }
                                if (!loadError && !compileError){
                                    programarNodosButton.setEnabled(true);
                                    pgStatusLabel.setText("Esperando...");
                                }
                            }
                        }.execute();
                    }else{
                        JOptionPane.showMessageDialog(frame, "Debe seleccionar un programa y un nodo de las listas.",
                                "Seleccione...", JOptionPane.INFORMATION_MESSAGE);
                    }
                }else{
                    JOptionPane.showMessageDialog(frame, "Debe seleccionar un programa y un nodo de las listas.",
                            "Seleccione...", JOptionPane.INFORMATION_MESSAGE);
                }

            }
        });
        tbPaneMain.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (tbPaneMain.getSelectedIndex() == 1){
                    moteJList.clearSelection();
                }
            }
        });
        checkContikiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findAndDownloadContiki(true);
            }
        });
    }

    /**
     * Busca la presencia de contiki y de no encontrarla lo descarga y descomprime.
     * @param skipQuestion booleano para evitar que pregunte si desea descargar
     */
    private void findAndDownloadContiki(boolean skipQuestion){
        File f = new File("contiki-3.0");
        if (!f.exists()) {
            contikiStatusLabel.setBackground(Color.RED);
            int answer;
            if(!skipQuestion) {
                Object[] options = {"Si, descargar",
                        "No, cerrar aplicación"};
                answer = JOptionPane.showOptionDialog(this,
                        "Este programa necesita Contiki para funcionar.\n¿Desea descargarlo?",
                        "Descargar Contiki",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[0]);
            }else {
                answer = 0;
            }
            if (answer == 0) { // 0 = Si
                try{
                    if (DEBUG){System.out.println("MW: Downloading.");}
                    DownloadDialog downloadDialog = new DownloadDialog(this);
                }catch(Exception ex) {
                    if (DEBUG) {
                        System.out.println("MW: Exception from MW.");
                        ex.printStackTrace();
                    }
                    int input = JOptionPane.showConfirmDialog(this,
                            "Error al tratar de descargar Contiki, por favor reinicie la aplicación.",
                            "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                    if (input == JOptionPane.OK_OPTION || input == JOptionPane.CLOSED_OPTION) {
                        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
                    }
                }
            } else {
                this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
            }
            populateComponents();
            contikiStatusLabel.setBackground(Color.GREEN);
        } else {
            contikiStatusLabel.setBackground(Color.GREEN);
            populateComponents();
        }
    }

    /**
     * Pobla los componentes de la interfaz.
     */
    private void populateComponents(){
        populateContikiJtree();
        populateProgramsList();
        populateMoteList();
        populatePgProgramTree();
        validate();
    }

    /**
     * Pobla la lista de programas de Contiki
     */
    private void populateContikiJtree(){
        leftApplicationPanel.setLayout(new BorderLayout());
        appFileTree = new FileTree(new File("contiki-3.0/examples"));
        leftApplicationPanel.add(appFileTree);
    }

    /**
     * Pobla la la lista de programas del usuario
     */
    private void populateProgramsList(){
        DefaultListModel<Program> listModel = new DefaultListModel<>();
        ArrayList<Program> programs = settingsHandler.getPrograms();
        if (programs != null) {
            for (Program p : programs) {
                listModel.addElement(p);
            }
        }
        programsList.setModel(listModel);
    }

    /**
     * Pobla la lista de motas
     */
    private void populateMoteList(){
        DefaultListModel<Mote> listModel = new DefaultListModel<>();
        try{
            motes = new ArrayList<>(Arrays.asList(moteFinder.getMotes()));
            if (motes != null){
                for (Mote m : motes){
                    listModel.addElement(m);
                }
            }
            moteJList.setModel(listModel);
            pgMoteList.setModel(listModel);
            pgResultList.setModel(listModel);
        }catch (IOException e){
            JOptionPane.showMessageDialog(this, "Error al leer lista de motas: " + e.getMessage(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Pobla la lista de programas de la pestaña programar
     */
    private void populatePgProgramTree(){
        DefaultMutableTreeNode appProgramsRoot = (DefaultMutableTreeNode) appFileTree.getJTree().getModel().getRoot();
        DefaultMutableTreeNode newRoot = new DefaultMutableTreeNode("Root");
        newRoot.add(appProgramsRoot);

        DefaultMutableTreeNode userProgramsNode = new DefaultMutableTreeNode("Programas del usuario");

        for (int i = 0; i < programsList.getModel().getSize(); i++){
            Program p = (Program) programsList.getModel().getElementAt(i);
            DefaultMutableTreeNode localNode = new DefaultMutableTreeNode(p);
            userProgramsNode.add(localNode);
        }
        if (userProgramsNode.getChildCount() > 0) {
            newRoot.add(userProgramsNode);
        }
        DefaultTreeModel tm = new DefaultTreeModel(newRoot);
        pgProgramsTree.setModel(tm);
        pgProgramsTree.setRootVisible(false);
    }

    /**
     * Elimina un directorio recursivamente.
     * @param directory el directorio a eliminar
     * @return el resultado de la eliminacion del directorio.
     */
    private boolean deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(files != null){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
        }
        return(directory.delete());
    }

    /**
     * Cierra la ventana
     */
    private void closeFrame(){
        super.dispose();
    }
}
