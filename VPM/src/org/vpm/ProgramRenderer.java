package org.vpm;

import javax.swing.*;
import java.awt.*;

/**
 * Clase encargada de crear una celda personalizada para utilizar en una lista.
 * Created by felipe on 5/13/16.
 */
public class ProgramRenderer extends JEditorPane implements ListCellRenderer<Program>{

    /**
     * Utiliza los datos del programa para crear una celda personalizada.
     */
    public ProgramRenderer(){
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(
            JList<? extends Program> list, Program program,
            int index,  boolean isSelected, boolean cellHasFocus){
        setContentType("text/html");
        setText("<html><b>"+program.getName()+"</b><br/>"+program.getPath());

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        return this;
    }
}
