package org.vpm;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Clase encargada de representar a un programa de contiki, guardando su ubicacion,
 * nombre y con estos datos poder compilar dicho programa.
 * Created by felipe on 5/13/16.
 */
public class Program {

    private String name, path;

    /**
     * Crea una nueva instancia de programa
     * @param programName Nombre del programa
     * @param programPath Ubicacion del programa
     */
    public Program(String programName, String programPath){
        name = programName;
        path = programPath;
    }

    /**
     * Obtiene el nombre del programa
     * @return Nombre del programa
     */
    public String getName() {
        return name;
    }

    /**
     * Asigna el nombre del programa
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Obtiene la ubicacion del programa en el sistema de archivos
     * @return Ubicacion del programa
     */
    public String getPath() {
        return path;
    }

    /**
     * Asigna la ubicacion en el sistema de archivos al programa
     * @param path Ubicacion del programa
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Compila el programa para posterior uso.
     * @throws IOException Al no poder iniciar el proceso.
     * @throws InterruptedException Al proceso ser interrumpido
     */
    public int compile() throws IOException, InterruptedException{
        ProcessBuilder pb = new ProcessBuilder("make",
                getName().replaceAll(".[cC]$",""),
                "TARGET=sky",
                "CONTIKI="+System.getProperty("user.dir")+"/contiki-3.0");

        pb.directory(new File(getPath().replaceFirst("([^\\/]+\\.[cC])$", "")));
        Process process = pb.start();


        final BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
        final BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));


        Thread readInput = new Thread(new Runnable() {
            @Override
            public void run() {
                String line;
                try {
                    while((line = input.readLine()) != null ){
                        System.out.println("IS: "+line);
                    }
                }catch (IOException e){
                    System.out.println("Excepeción lanzada al tratar de leer el buffer de entrada.");
                }
            }
        });

        Thread readError = new Thread(new Runnable() {
            @Override
            public void run() {
                String line;
                try{
                    while ((line = error.readLine()) != null){
                        System.out.println("ES: "+line);
                    }
                }catch (IOException e){
                    System.out.println("Excepción lanzada al tratar de leer el stream de error.");
                }
            }
        });

        readInput.start();
        readError.start();

        readInput.join();
        process.waitFor();
        return process.exitValue();
    }

    /**
     * Devuelve la representacion del objeto en formato string.
     * @return Representacion en String del objeto.
     */
    @Override
    public String toString(){
        return name;
    }
}
