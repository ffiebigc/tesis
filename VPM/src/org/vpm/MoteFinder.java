package org.vpm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Clase encargada de proporcionar la lista de motas o nodos conectados al
 * equipo utilizando las herramientas de contiki.
 * Created by felipe on 5/23/16.
 */
public class MoteFinder {

    private static final String MOTELIST = "/contiki-3.0/tools/sky/motelist-linux";

    private ArrayList<Mote> moteList = new ArrayList<>();
    private Process motelistProcess;
    private final Pattern motePattern;

    /**
     * Instancia un objecto de la clase
     */
    public MoteFinder(){
        motePattern = Pattern.compile("([^\\,]+)\\,([^\\,]+)+\\,([^\\,]+)");
    }

    /**
     * Busca y devuelve la lista de motas en forma de arreglo.
     * @return la lista de motas
     * @throws IOException
     */
    public Mote[] getMotes() throws IOException{
        findMotes();
        return moteList.toArray(new Mote[moteList.size()]);
    }

    /**
     * Ejecuta el proceso para obtener la lista de motas.
     * @throws IOException
     */
    private void findMotes() throws IOException {
        moteList.clear();
        String completeCommand = System.getProperty("user.dir")+MOTELIST;
        try {
            String[] cmd = new String[] {completeCommand, "-c"};
            motelistProcess = Runtime.getRuntime().exec(cmd);
            final BufferedReader input = new BufferedReader(new InputStreamReader(motelistProcess.getInputStream()));
            final BufferedReader error = new BufferedReader(new InputStreamReader(motelistProcess.getErrorStream()));

            Thread readInput = new Thread(new Runnable() {
                @Override
                public void run() {
                    String line;
                    try {
                        while ((line = input.readLine()) != null ){
                            parseLine(line);
                        }
                    }catch (IOException e){
                        System.err.println("Excepcion lanzada al tratar de leer la lista de motas");
                    }
                }
            }, "Leer hilo de lista de motas");

            Thread readError = new Thread(new Runnable() {
                @Override
                public void run() {
                    String line;
                    try {
                        while((line = error.readLine()) != null){
                            System.err.println("Errores de motelist: "+line);
                        }
                        error.close();
                    }catch (IOException e){
                        System.err.println("Excepcion lanzada al leer desde stream de errores de lista de motas: " + e);
                    }
                }
            }, "Leer hilo de errores de lista de motas");

            readInput.start();
            readError.start();

            readInput.join();
        } catch (Exception e){
            throw (IOException) new IOException("Error al ejecutar '"+ completeCommand +"'").initCause(e);
        }
    }

    /**
     * Analiza el string entregado y si coincide con la expresion regular instancia una mota con la informacion.
     * Este metodo debe ser usado dentro del metodo findMotes() para que funcione.
     * @param line la linea en formato string
     */
    private void parseLine(String line){
        if (line.contains("No devices found")){
            //No hay nodos conectados
        }else {
            Matcher matcher = motePattern.matcher(line);
            if (matcher.find()){
                String comPort = matcher.group(2);
                String desc = matcher.group(3);
                moteList.add(new Mote(comPort, desc));
            }else {
                System.err.println("Motelist:" + line);
            }
        }
    }

    /**
     * Destruye el proceso encargado de encontrar motas.
     */
    public void close(){
        if (motelistProcess != null){
            motelistProcess.destroy();
            motelistProcess = null;
        }
    }
}