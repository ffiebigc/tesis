package org.vpm;

import javax.swing.*;
import java.awt.*;

/**
 * Clase encargada de crear una celda personalizada para utilizar en una lista.
 * Created by felipe on 6/8/16.
 */
public class ResultRenderer extends JEditorPane implements ListCellRenderer<Mote>{

    /**
     * Utiliza los datos de las motas para crear una celda personalizada mostrando el resultado de la compilación.
     */
    public ResultRenderer(){
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(
            JList<? extends Mote> list, Mote mote,
            int index,  boolean isSelected, boolean cellHasFocus){
        setContentType("text/html");
        String status = "";
        switch (mote.getStatus()){
            case Mote.WAITING:
                status = "Esperando";
                break;
            case Mote.BEING_PROGRAMMED:
                status = "Siendo programada";
                break;
            case Mote.FAILED:
                status = "Error";
                break;
            case Mote.RETRY:
                status = "Intente nuevamente";
                break;
        }
        setText("<html><b>"+mote.getDevice()+"</b><br/>"+status);

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        return this;
    }
}