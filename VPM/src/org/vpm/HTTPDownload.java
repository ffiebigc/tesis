package org.vpm;

import java.io.*;
import org.vpm.gui.DownloadDialog;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import javax.swing.*;

/**
 * Clase encargada de la conexión HTTP, descarga y descompresión de contiki.
 * Created by felipe on 5/6/16.
 */
public class HTTPDownload extends SwingWorker<Void, Void>{

    private static int BUFFER_SIZE = 4096;
    private HttpURLConnection httpCon;
    private String fileName;
    private int contentLength;
    private String fileUrl, savePath;
    private boolean DEBUG = true;
    private DownloadDialog parent;

    /**
     * Constructor de la clase.
     * @param parent el componente padre utilizado para mostrar ventanas emergentes.
     * @param fileUrl url del archivo a descargar.
     * @param savePath ubicacion de destino del archivo.
     */
    public HTTPDownload(DownloadDialog parent, String fileUrl, String savePath){
        this.fileUrl = fileUrl;
        this.savePath = savePath;
        this.parent = parent;
    }

    /**
     * Descarga el archivo estipulado en el constructor en un hilo aparte para no bloquear el EDT.
     *
     * @throws IOException Al no poder conectarse al archivo.
     * @see IOException
    */
    public Void doInBackground() throws IOException{
        URL url = new URL(fileUrl);
        httpCon = (HttpURLConnection) url.openConnection();
        int responseCode = httpCon.getResponseCode();


        if (DEBUG) {System.out.println("HTTPDw: Response Code: "+responseCode);}
        //Verificar si la respuesta HTTP es la correcta.
        if (responseCode == HttpURLConnection.HTTP_OK){
            String disposition = httpCon.getHeaderField("Content-Disposition");
            String contentType = httpCon.getContentType();
            contentLength = httpCon.getContentLength();

                if (disposition != null) {
                    // Extrae el nombre del Header HTTP.
                    int index = disposition.indexOf("filename=");
                    if (index > 0) {
                        fileName = disposition.substring(index + 9,
                                disposition.length());
                    }else {
                        // Extrae el nombre desde el URL
                        fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1,
                                fileUrl.length());
                    }
                    if (DEBUG) {
                        System.out.println("HTTPDw: Content-Type = " + contentType);
                        System.out.println("HTTPDw: Content-Disposition = " + disposition);
                        System.out.println("HTTPDw: Content-Length = " + contentLength);
                        System.out.println("HTTPDw: fileName = " + fileName+"\n");
                    }

                    InputStream inputStream = httpCon.getInputStream();

                    FileOutputStream outputStream = new FileOutputStream(savePath);

                    byte[] buffer = new byte[BUFFER_SIZE];
                    int bytesRead = -1;
                    long totalBytesRead = 0;
                    int percentCompleted = 0;
                    long fileSize = contentLength;


                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                        totalBytesRead += bytesRead;
                        if (contentLength > 0) {
                            percentCompleted = (int) (totalBytesRead * 100 / fileSize);
                            setProgress(percentCompleted);
                        }
                        if (DEBUG) {
                            System.out.flush();
                            System.out.print("\rHTTPDw: Progress: " + getProgress() + "%");
                        }
                    }

                    outputStream.close();
                    inputStream.close();

                    if (DEBUG) {System.out.println("\nHTTPDw: Archivo descargado.");}
                }

        }else{
            throw new IOException(
                    "No se pudo descargar el archivo. Código HTTP: "
                            + responseCode);
        }
        httpCon.disconnect();
        return null;
    }

    /**
     *Se ejecuta al terminar el proceso en background estipulado en doInBackground().
     */
    @Override
    protected void done() {
        try {
            get();
        } catch (ExecutionException e){
            if (DEBUG){
                e.getCause().printStackTrace();
                String msg = String.format("Problema inesperado: %s",
                        e.getCause().toString());
                System.out.println(msg);
            }
            int input = JOptionPane.showConfirmDialog(parent, "Error al tratar de descargar Contiki, por favor reinicie la aplicación.",
                    "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == JOptionPane.OK_OPTION || input == JOptionPane.CLOSED_OPTION) {
                parent.sendCloseSignal();
            }
        } catch (InterruptedException e){
            if (DEBUG) {
                e.getCause().printStackTrace();
                String msg = String.format("Problema inesperado: %s",
                        e.getCause().toString());
                System.out.println(msg);
            }
            int input = JOptionPane.showConfirmDialog(parent, "Error al tratar de descargar Contiki, por favor reinicie la aplicación.",
                    "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == JOptionPane.OK_OPTION || input == JOptionPane.CLOSED_OPTION) {
                parent.sendCloseSignal();
            }
        }
        parent.setBottomText("Descomprimiendo...");
        if (DEBUG){System.out.println("HTTPDw: Unziping.");}
        if (new File("3.0.zip").exists()){
            String cmd = "unzip";
            String args = "3.0.zip";
            try {
                ProcessBuilder pb = new ProcessBuilder(cmd, args);
                pb.directory(new File(System.getProperty("user.dir")));
                Process process = pb.start();

                final BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
                final BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));

                Thread readInput = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String line;
                        try {
                            while ((line = input.readLine()) != null) {
                                System.out.println("IS: "+line);
                            }
                        } catch (IOException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                });

                Thread readError = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String line;
                        try {
                            while ((line = error.readLine()) != null) {
                                System.out.println("ES: "+line);
                            }
                        } catch (IOException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                });

                readInput.start();
                readError.start();

                readInput.join();
                readError.join();

                process.waitFor();

                File zip = new File("3.0.zip");
                zip.delete();
                parent.enableOkButton(true);
                parent.progressBar.setIndeterminate(false);
                parent.progressBar.setValue(100);
            }catch (IOException e){
                //Falló iniciar el proceso
                File directory = new File("contiki-3.0");
                if (directory.exists()) {
                    deleteDirectory(directory);
                }
                int input = JOptionPane.showConfirmDialog(parent,
                        "No se pudo descomprimir, porfavor reinicie la aplicacion.",
                        "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                if (input == JOptionPane.OK_OPTION || input == JOptionPane.CLOSED_OPTION){
                    parent.sendCloseSignal();
                }
            }catch (InterruptedException iex){
                //Proceso o hilo(s) interrumpido(s)
                File directory = new File("contiki-3.0");
                if (directory.exists()) {
                    deleteDirectory(directory);
                }
                int input = JOptionPane.showConfirmDialog(parent, "Descompresion interrumpida, reinicie la aplicacion.",
                        "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                if (input == JOptionPane.OK_OPTION || input == JOptionPane.CLOSED_OPTION){
                    parent.sendCloseSignal();
                }
            }
        }else{
            int input = JOptionPane.showConfirmDialog(parent,
                    "No se pudo descomprimir, porfavor reinicie la aplicacion.",
                    "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == JOptionPane.OK_OPTION || input == JOptionPane.CLOSED_OPTION){
                parent.sendCloseSignal();
            }
        }
    }

    /**
     * Elimina un directorio recursivamente.
     * @param directory el directorio a eliminar
     * @return el resultado de la eliminacion del directorio.
     */
    private boolean deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(files != null){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
        }
        return(directory.delete());
    }
}
