package org.vpm;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by felipe on 5/25/16.
 */
public class Mote {

    private String device, description;
    private int status;
    public final static int WAITING = 0;
    public final static int BEING_PROGRAMMED = 1;
    public final static int FAILED = -1;
    public final static int RETRY = -2;

    /**
     * Crea una instancia de la clase, que representa una mota.
     * @param device el dispositivo serial en que la mota esta conectada, ejemplo: /dev/ttyUSB0
     * @param description el nombre de la mota conectada
     */
    public Mote(String device, String description){
        this.device = device;
        this.description = description;
        setStatus(this.WAITING);
    }

    /**
     * Crea una instancia de la clase copiando otra instancia
     * @param ref la mota de referencia
     */
    public Mote (Mote ref){
        this.device = ref.device;
        this.description = ref.description;
        this.status = ref.status;
    }

    /**
     * Subir codigo compilado a la mota
     * @param program el programa que será cargado en la mota
     * @return el valor de retorno del proceso de carga
     */
    public int program(Program program){
        String dir = program.getPath().replaceFirst("([^\\/]+\\.[cC])$", "");
        File workingDirectory = new File(dir);
        String programNoExt = program.getName().replaceAll(".[cC]$", "");
        String strMoteNumber = this.device.replaceFirst("\\/dev\\/ttyUSB", "");
        int moteNumber = Integer.parseInt(strMoteNumber)+1;

        try {
            ProcessBuilder pb = new ProcessBuilder("make", programNoExt+".upload", "MOTE="+moteNumber, "TARGET=sky",
                    "CONTIKI="+System.getProperty("user.dir")+"/contiki-3.0");
            pb.directory(workingDirectory);

            Process process = pb.start();
            setStatus(this.BEING_PROGRAMMED);
            final BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            final BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            Thread readInput = new Thread(new Runnable() {
                @Override
                public void run() {
                    String line;
                    try {
                        while ((line = input.readLine()) != null) {
                            System.out.println("IS: "+line);
                        }
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                }
            });

            Thread readError = new Thread(new Runnable() {
                @Override
                public void run() {
                    String line;
                    CharSequence errString = "An error occoured:";
                    try {
                        while ((line = error.readLine()) != null) {
                            if (line.contains(errString)){
                                System.out.println("ES: "+line);
                                setStatus(Mote.FAILED);
                            }else{
                                System.out.println("ES: "+line);
                            }
                        }
                    } catch (IOException e) {
                        System.err.println(e.getMessage());
                    }
                }
            });

            readInput.start();
            readError.start();

            readInput.join();
            readError.join();

            process.waitFor();

            if (getStatus() == Mote.FAILED) {
                setStatus(Mote.RETRY);
            } else if (getStatus() == Mote.BEING_PROGRAMMED) {
                setStatus(Mote.WAITING);
            }
            System.out.println(process.exitValue());
            return process.exitValue();
        } catch (Exception e) {
            return -1;
        }
    }

    /**
     * Asigna el valor de estado de la mota
     * @param status el valor del estado, se deben utilizar las variables estaticas.
     */
    public void setStatus(int status){
        this.status = status;
    }

    /**
     * Recupera el valor de estado de la mota
     * @return
     */
    public int getStatus(){
        return status;
    }

    /**
     * Recupera el nombre del puerto serial en que la mota está conectada.
     * @return
     */
    public String getDevice() {
        return device;
    }

    /**
     * Asigna el nombre del puerto serial en que la mota está conectada.
     * @param device el nombre del puerto a asignar
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * Recupera el nombre de la mota
     * @return el nombre de la mota
     */
    public String getDescription() {
        return description;
    }

    /**
     * Asigna el nombre de la mota
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Devuelve la representacion del objeto en formato string.
     * @return Representacion en String del objeto.
     */
    @Override
    public String toString(){
        return "[Mote: Dev=\"" + device + "\" Desc=\"" + description +"\"]";
    }
}
