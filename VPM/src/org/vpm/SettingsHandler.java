package org.vpm;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase encargada de almacenar y proveer configuraciones del usuario, en la version
 * prototipo se guardan la ubicacion de los programas del usuario.
 * Created by felipe on 5/19/16.
 */
public class SettingsHandler {

    private Document dom;
    private Node root;

    /**
     *  Crea una instancia del manejador de configuraciones, si existe un archivo de configuracion
     *  lo lee y activa los cambios que su contenido genera. Si no existe un archivo lo crea para utilizarlo.
     */
    public SettingsHandler(){
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        
        File settingsXml = new File(System.getProperty("user.dir")+"/settings.xml");
        if (settingsXml.exists()){
            try{
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                dom = dBuilder.parse(settingsXml);
                dom.getDocumentElement().normalize();
                root = dom.getElementsByTagName("Settings").item(0);
            }catch (ParserConfigurationException parserException){
                System.err.println("Parser Exception");
            }catch (SAXException saxException){
                System.err.println("SAX Exception");
            }catch (IOException ioException){
                System.err.println("IO Exception");
            }
        }else{
            try {
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                dom = dBuilder.newDocument();
                root = dom.createElement("Settings");
                root.appendChild(dom.createElement("Programs"));
                dom.appendChild(root);
            }catch (ParserConfigurationException parserException){
                System.err.println("ParseConfigurationException");
            }
        }
    }

    /**
     * Agrega un programa al final de la lista de programas
     * @param program el programa que se agregará a la lista
     */
    public void appendProgram(Program program){
        Element programsElement = (Element) getNode("Programs", root.getChildNodes());
        Element newProgram = dom.createElement("Program");
        Element name = dom.createElement("Name");
        Element path = dom.createElement("Path");
        name.setTextContent(program.getName());
        path.setTextContent(program.getPath());
        newProgram.appendChild(name);
        newProgram.appendChild(path);
        programsElement.appendChild(newProgram);
        resetProgramsId();
        writeXML();
    }

    /**
     * Elimina un programa basado en su posicion en la lista de programas.
     * @param index la posicion del programa a remover
     */
    public void removeProgram(int index){
        Element programsElement = (Element) getNode("Programs", root.getChildNodes());
        NodeList nodeList = programsElement.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++){
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE){
                Element localNode = (Element) nodeList.item(i);
                int id = Integer.parseInt(localNode.getAttribute("id"));
                if (id == index){
                    programsElement.removeChild(nodeList.item(i));
                }
            }
        }
        resetProgramsId();
        writeXML();
    }

    /**
     * Obtiene la lista de programas desde el XML.
     * @return la lista de programas
     */
    public ArrayList<Program> getPrograms(){
        Element programsElement = (Element) getNode("Programs", root.getChildNodes());
        NodeList nodeList = programsElement.getChildNodes();
        ArrayList<Program> programs = new ArrayList<>();
        int idCount = 0;
        if (nodeList != null) {
            for (int i = 0; i < nodeList.getLength(); i++) {
                if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element p = (Element) nodeList.item(i);
                    String name, path;
                    NodeList datos = p.getChildNodes();
                    if (datos != null) {
                        name = datos.item(1).getTextContent();
                        path = datos.item(3).getTextContent();
                        if ((new File(path).exists())){
                            programs.add(new Program(name, path));
                            idCount++;
                        }else{
                            removeProgram(idCount);
                        }
                    }
                }
            }
            return programs;
        }else {
            return null;
        }
    }

    /**
     * Reasigna los ids de los programas segun el orden en que esten en el XML.
     * Se utiliza luego de eliminar o agregar programas.
     */
    private void resetProgramsId(){
        Element programsElement = (Element) getNode("Programs", root.getChildNodes());
        NodeList nodeList = programsElement.getChildNodes();
        int programCount = 0;
        if (nodeList != null){
            for(int i = 0; i < nodeList.getLength(); i++){
                if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element program = (Element) nodeList.item(i);
                    program.setAttribute("id", programCount + "");
                    programCount++;
                }
            }
        }
    }

    /**
     * Escribe el xml presente en la memoria en el disco.
     */
    private void writeXML(){
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try{
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(dom);
            StreamResult result = new StreamResult(new File(System.getProperty("user.dir")+"/settings.xml"));
            transformer.transform(source, result);
        }catch (TransformerConfigurationException tcEx){
            System.err.println("TransformerConfigurationException");
        }catch (TransformerException tEx){
            System.err.println("TransformerException");
        }


    }

    /**
     * Obtiene el nodo que tenga el tag provisto dentro de la lista de nodos entregada.
     * @param tagName el tag a buscar
     * @param nodes la lista de nodos en los que buscar
     * @return el nodo resultante
     */
    private Node getNode(String tagName, NodeList nodes){
        for(int i = 0; i < nodes.getLength(); i++){
            Node node = nodes.item(i);
            if (node.getNodeName().equalsIgnoreCase(tagName)){
                return node;
            }
        }
        return null;
    }
}
