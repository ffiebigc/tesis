package org.vpm;

import javax.swing.*;
import java.awt.*;

/**
 * Clase encargada de crear una celda personalizada para utilizar en una lista.
 * Created by felipe on 5/27/16.
 */
public class MoteRenderer extends JEditorPane implements ListCellRenderer<Mote>{

    /**
     * Utiliza los datos de las motas para crear una celda personalizada.
     */
    public MoteRenderer(){
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(
            JList<? extends Mote> list, Mote mote,
            int index,  boolean isSelected, boolean cellHasFocus){
        setContentType("text/html");
        setText("<html><b>"+mote.getDescription()+"</b><br/>"+mote.getDevice());

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        return this;
    }
}
