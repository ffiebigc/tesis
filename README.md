# Sistema administrativo de redes de sensores basadas en 6LoWPAN
---
### Requisitos
	- apt-get ant install build-essential git binutils-msp430 msp430mcu gcc-msp430 msp430-libc mspdebug unzip
	- Descomprimir Contikies.tar.gz
	- Ejecutar "sudo usermod -a -G dialout [user-name]", reboot
	- Instalar java 8
		$ sudo add-apt-repository ppa:webupd8team/java
		$ sudo apt-get update
		$ sudo apt-get install oracle-java8-installer
        $ sudo apt-get install oracle-java8-set-default
    - Bajar y configurar IntelliJ Community Edition para usar JDK8 al importar el proyecto, esperar la indexacion.
    - Ejecutar Main